package logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AmpexLogger implements IAmpexLogger
{
    private Logger logger = null;

    public static void defaultConfigureLog4J()
    {
        System.setProperty("log4j.configurationFile", "ALImplog4j.xml");
    }

    public AmpexLogger(String _name)
    {
        logger = LogManager.getLogger(_name);
    }

    @Override
    public void trace(String _message)
    {
        logger.trace(_message);
    }

    @Override
    public void info(String _message)
    {
        logger.info(_message);
    }

    @Override
    public void debug(String _message)
    {
        logger.debug(_message);
    }

    @Override
    public void warn(String _message)
    {
        logger.warn(_message);
    }

    @Override
    public void warn(String _message, Throwable _throwable)
    {
        logger.warn(_message, _throwable);
    }

    @Override
    public void warn(Throwable _throwable)
    {
        logger.warn(_throwable);
    }

    @Override
    public void error(String _message)
    {
        logger.error(_message);
    }

    @Override
    public void error(String _message, Throwable _throwable)
    {
        logger.error(_message, _throwable);
    }

    @Override
    public void error(Throwable _throwable)
    {
        logger.error(_throwable);
    }

    @Override
    public void fatal(String _message)
    {
        logger.fatal(_message);
    }

    @Override
    public void fatal(String _message, Throwable _throwable)
    {
        logger.fatal(_message, _throwable);
    }

    @Override
    public void fatal(Throwable _throwable)
    {
        logger.fatal(_throwable);
    }
}
